package com.ipl.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IplMsManageUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(IplMsManageUsersApplication.class, args);
	}

}
