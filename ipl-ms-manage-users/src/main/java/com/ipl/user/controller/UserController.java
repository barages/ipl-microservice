package com.ipl.user.controller;

import com.ipl.user.model.User;
import com.ipl.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllMatches() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUser(@PathVariable Integer id) {
        Optional<User> optionalMatchDetails = userRepository.findById(id);

        User user = optionalMatchDetails.orElseThrow(() -> new IllegalArgumentException("Invalid id " + id));
        return ResponseEntity.ok(user);
    }

    @PostMapping("/users")
    public ResponseEntity<User> createUser(@RequestBody User user) {
        User savedMatchDetails = userRepository.saveAndFlush(user);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").build(Map.of("id", savedMatchDetails.getUserId()));

        return ResponseEntity.created(uri).body(savedMatchDetails);
    }

    @DeleteMapping("/users/{id}")
    public void deleteMatch(@PathVariable Integer id) {
        userRepository.deleteById(id);
    }


    @PutMapping("/users/{id}")
    public ResponseEntity<Void> updateResult(@PathVariable Integer id, @RequestBody User user) {
        userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid id " + id));

        userRepository.saveAndFlush(user);

        return ResponseEntity.ok().build();
    }

}
