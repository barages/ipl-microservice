package com.ipl.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IplMsLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(IplMsLoginApplication.class, args);
	}

}
