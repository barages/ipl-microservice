package com.ipl.teams.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.io.File;

@Data
@AllArgsConstructor
@Builder
public class IplTeamDetailsRequest {

    @ApiModelProperty(required = true, example = "Chennai Super King")
    @NotBlank(message = "Parameter 'teamName' is required")
    private String teamName;

    @ApiModelProperty(required = false)
    private MultipartFile teamAvatar;

}
