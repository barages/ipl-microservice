package com.ipl.teams.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Contains ipl teams details")
public class IplTeamDetailsResponse {

    private String teamName;
    private byte[] teamAvatar;
    private int teamId;
}
