package com.ipl.teams.services;

import com.ipl.teams.model.IplTeamDetailsRequest;
import com.ipl.teams.model.IplTeamDetailsResponse;
import com.ipl.teams.model.TeamDetails;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class IplTeamDaoImpl implements IplTeamDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List getIplTeamsDetails() {

        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TeamDetails.class);

        return criteria.list();
    }

    @Override
    public void createIplTeam(IplTeamDetailsRequest iplTeamDetailsRequest) throws Exception{
        try {
            TeamDetails teamDetails = new TeamDetails();
            teamDetails.setTeam_name(iplTeamDetailsRequest.getTeamName());
            byte[] file = null;
            if(iplTeamDetailsRequest.getTeamAvatar() !=null && !iplTeamDetailsRequest.getTeamAvatar().isEmpty()){
                file = iplTeamDetailsRequest.getTeamAvatar().getBytes();}

            teamDetails.setAvatar(file);
            entityManager.persist(teamDetails);
        }catch (Exception e)
        {
            throw e;
        }
    }

    @Override
    public void deleteIplTeam(int teamId) throws Exception{
        try {
            TeamDetails teamDetails = entityManager.find(TeamDetails.class, teamId);
            teamDetails.setTeam_id(teamId);
            entityManager.remove(teamDetails);
        }catch (Exception e)
        {
            throw e;
        }
    }
}
