package com.ipl.teams.services;

import com.ipl.teams.model.IplTeamDetailsRequest;
import com.ipl.teams.model.IplTeamDetailsResponse;

import java.util.List;

public interface IplTeamDao {

    List getIplTeamsDetails();
    void createIplTeam(IplTeamDetailsRequest iplTeamDetailsRequest)throws Exception;
    void deleteIplTeam(int teamId) throws Exception;
}
