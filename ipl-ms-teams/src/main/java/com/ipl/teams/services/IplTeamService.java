package com.ipl.teams.services;

import com.ipl.teams.model.IplTeamDetailsRequest;
import com.ipl.teams.model.IplTeamDetailsResponse;

import java.util.List;


public interface IplTeamService {

    List<IplTeamDetailsResponse> getIplTeamsDetails();
    String createIplTeam(IplTeamDetailsRequest iplTeamDetailsRequest)throws Exception;
    String deleteIplTeam(int teamId) throws Exception;
}
