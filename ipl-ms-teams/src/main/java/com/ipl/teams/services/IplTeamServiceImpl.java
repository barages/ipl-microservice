package com.ipl.teams.services;

import com.ipl.teams.model.IplTeamDetailsRequest;
import com.ipl.teams.model.IplTeamDetailsResponse;
import com.ipl.teams.model.TeamDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class IplTeamServiceImpl implements IplTeamService{

    @Autowired
    IplTeamDao iplTeamDao;

    @Override
    public List<IplTeamDetailsResponse> getIplTeamsDetails() {
       List<IplTeamDetailsResponse> iplTeamDetailsResponseList = new ArrayList<>();



        List<TeamDetails> teamDetails = iplTeamDao.getIplTeamsDetails();

        for (TeamDetails teamDetails1 : teamDetails) {
            IplTeamDetailsResponse iplTeamDetailsResponse = new IplTeamDetailsResponse();
            iplTeamDetailsResponse.setTeamName(teamDetails1.getTeam_name());
            iplTeamDetailsResponse.setTeamAvatar(teamDetails1.getAvatar());
            iplTeamDetailsResponse.setTeamId(teamDetails1.getTeam_id());
            iplTeamDetailsResponseList.add(iplTeamDetailsResponse);
        }

        return iplTeamDetailsResponseList;

    }

    @Override
    public String createIplTeam(IplTeamDetailsRequest iplTeamDetailsRequest) throws Exception{
        String createStatus = null;
        try {
            iplTeamDao.createIplTeam(iplTeamDetailsRequest);
            createStatus = "Team "+ iplTeamDetailsRequest.getTeamName()+" Added Successfully";
        }catch (Exception e)
        {
            createStatus = "Failed to add";
            throw e;
        }
        return createStatus;
    }

    @Override
    public String deleteIplTeam(int teamId) throws Exception{

        String deleteStatus = null;
        try {
            iplTeamDao.deleteIplTeam(teamId);
            deleteStatus = "Team "+ teamId +" Deleted Successfully";
        }catch (Exception e)
        {
            deleteStatus = "Failed to delete";
            throw e;
        }
        return deleteStatus;
    }
}
