package com.ipl.teams.controller;

import com.ipl.teams.model.IplTeamDetailsRequest;
import com.ipl.teams.model.IplTeamDetailsResponse;
import com.ipl.teams.services.IplTeamService;
import com.ipl.teams.services.IplTeamServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@RestController
@RequestMapping("v1/ipl")
public class IplTeamsController {

    @Autowired
    IplTeamService iplTeamService;

    @ApiOperation(value = "list", notes = "Returns the list of team palying in IPL")
    @ApiResponses(value  =
            {
                    @ApiResponse(code = 200, message = "Successful", response = List.class),
                    @ApiResponse(code = 400, message = "Bad Request", response = Exception.class),
                    @ApiResponse(code = 403, message = "Forbidden", response = Exception.class),
                    @ApiResponse(code = 404, message = "Not Found", response = Exception.class),
                    @ApiResponse(code = 500, message = "Technical Error", response = Exception.class)
            })
    @GetMapping("/teams")
    public ResponseEntity getTeams() throws Exception {
        //iplTeamService = new IplTeamServiceImpl();
        List<IplTeamDetailsResponse> iplTeamDetailsResponseList = iplTeamService.getIplTeamsDetails();
        return new ResponseEntity(iplTeamDetailsResponseList, HttpStatus.OK);
    }

    @ApiOperation(value = "string", notes = "Insert team palying in IPL")
    @ApiResponses(value  =
            {
                    @ApiResponse(code = 200, message = "Successful", response = String.class),
                    @ApiResponse(code = 400, message = "Bad Request", response = Exception.class),
                    @ApiResponse(code = 403, message = "Forbidden", response = Exception.class),
                    @ApiResponse(code = 404, message = "Not Found", response = Exception.class),
                    @ApiResponse(code = 500, message = "Technical Error", response = Exception.class)
            })
    @PostMapping("/addteam")
    public ResponseEntity addTeam(@RequestParam(value = "teamName", required = true) String teamName,
                                  @RequestParam(value = "avatar", required = false) MultipartFile avatarFile) throws Exception {
        IplTeamDetailsRequest iplTeamDetailsRequest = new IplTeamDetailsRequest(teamName, avatarFile);

        String iplTeamCreateResponse = iplTeamService.createIplTeam(iplTeamDetailsRequest);
        return new ResponseEntity(iplTeamCreateResponse, HttpStatus.OK);
    }

    @ApiOperation(value = "string", notes = "Delete team palying in IPL")
    @ApiResponses(value  =
            {
                    @ApiResponse(code = 200, message = "Successful", response = String.class),
                    @ApiResponse(code = 400, message = "Bad Request", response = Exception.class),
                    @ApiResponse(code = 403, message = "Forbidden", response = Exception.class),
                    @ApiResponse(code = 404, message = "Not Found", response = Exception.class),
                    @ApiResponse(code = 500, message = "Technical Error", response = Exception.class)
            })
    @DeleteMapping("/deleteteam")
    public ResponseEntity deleteteam(@RequestParam(value = "teamId", required = true) int teamId) throws Exception {

        String iplTeamCreateResponse = iplTeamService.deleteIplTeam(teamId);
        return new ResponseEntity(iplTeamCreateResponse, HttpStatus.OK);
    }

}
