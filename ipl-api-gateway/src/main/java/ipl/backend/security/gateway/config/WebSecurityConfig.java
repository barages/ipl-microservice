package ipl.backend.security.gateway.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import ipl.backend.security.gateway.common.JwtAuthenticationConfig;
import ipl.backend.security.gateway.common.JwtLoginFilter;
import ipl.backend.security.gateway.common.JwtTokenAuthenticationFilter;
import ipl.backend.security.gateway.services.UserDetailServiceImpl;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationConfig jwtConfig;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Bean
	public JwtAuthenticationConfig jwtConfig() {
		return new JwtAuthenticationConfig();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailServiceImpl();
	};

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService());
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().logout().disable().formLogin().disable().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().anonymous().and().exceptionHandling()
				.authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)).and()
				// We filter the /login requests - authenticate the user and issue the JWT token to client
				.addFilterBefore(
					new JwtLoginFilter(jwtConfig, authenticationManager),
					UsernamePasswordAuthenticationFilter.class
				)
				// this filters other requests, finds and validates the JWS token in request
				.addFilterBefore(new JwtTokenAuthenticationFilter(jwtConfig),
						UsernamePasswordAuthenticationFilter.class)

				.authorizeRequests()
				.antMatchers(jwtConfig.getUrl()).permitAll()
				.antMatchers("/**/admin/**").hasRole("ADMIN")
				.antMatchers("/**/user/**").hasRole("USER")
				.antMatchers("/restful/**").hasRole("USER")
				.antMatchers("/**/guest/**").permitAll();
	}
}
