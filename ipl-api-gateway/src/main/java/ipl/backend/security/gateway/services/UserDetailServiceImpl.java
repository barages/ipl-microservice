package ipl.backend.security.gateway.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ipl.backend.security.gateway.dao.UserRepository;
import ipl.backend.security.gateway.model.User;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        UserBuilder builder = null;

        if (user == null) {
            throw new BadCredentialsException("Invalid username or password.");
        }
        
        builder = org.springframework.security.core.userdetails.User.withUsername(user.getEmail());
        builder.password("{noop}" + user.getPassword());
        
        builder.roles(user.isAdmin() ? new String[] {"ADMIN", "USER"} : new String[] {"USER"});

        return builder.build();
    }
}
