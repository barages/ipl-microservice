package com.ipl.matches.repository;

import com.ipl.matches.model.MatchDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchesRepository extends JpaRepository<MatchDetails, Integer> {
}
