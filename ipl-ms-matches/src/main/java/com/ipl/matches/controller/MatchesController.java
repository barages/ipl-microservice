package com.ipl.matches.controller;

import com.ipl.matches.model.MatchDetails;
import com.ipl.matches.model.MatchResultStatus;
import com.ipl.matches.repository.MatchesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class MatchesController {

    @Autowired
    private MatchesRepository matchesRepository;

    @GetMapping("/matches")
    public ResponseEntity<List<MatchDetails>> getAllMatches() {
        return ResponseEntity.ok(matchesRepository.findAll());
    }

    @GetMapping("/matches/{id}")
    public ResponseEntity<MatchDetails> getMatchDetails(@PathVariable Integer id) {
        Optional<MatchDetails> optionalMatchDetails = matchesRepository.findById(id);

        MatchDetails matchDetails = optionalMatchDetails.orElseThrow(() ->  new IllegalArgumentException("Invalid id " + id));
        return ResponseEntity.ok(matchDetails);
    }

    @PostMapping("/matches")
    public ResponseEntity<MatchDetails> createMatch(@RequestBody MatchDetails requestMatchDetails) {
        MatchDetails savedMatchDetails = matchesRepository.saveAndFlush(requestMatchDetails);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").build(Map.of("id", savedMatchDetails.getMatchId()));

        return ResponseEntity.created(uri).body(savedMatchDetails);
    }

    @DeleteMapping("/matches/{id}")
    public void deleteMatch(@PathVariable Integer id) {
        matchesRepository.deleteById(id);
    }

    @PutMapping("/matches/{id}")
    public ResponseEntity<Void> rescheduleMatch(@PathVariable Integer id, @RequestBody MatchDetails requestMatchDetails) {
        Optional<MatchDetails> optionalMatchDetails = matchesRepository.findById(id);

        MatchDetails matchDetails = optionalMatchDetails.orElseThrow(() ->  new IllegalArgumentException("Invalid id " + id));
        matchDetails.setStartTime(requestMatchDetails.getStartTime());

        matchesRepository.saveAndFlush(requestMatchDetails);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/matches/{id}/result_status/{resultStatus}")
    public ResponseEntity<Void> updateResult(@PathVariable Integer id, @PathVariable String resultStatus) {
        Optional<MatchDetails> optionalMatchDetails = matchesRepository.findById(id);

        MatchDetails matchDetails = optionalMatchDetails.orElseThrow(() ->  new IllegalArgumentException("Invalid id " + id));

        MatchResultStatus matchResultStatus = MatchResultStatus.valueOf(resultStatus);

        if (matchResultStatus == MatchResultStatus.ABANDONED || matchResultStatus == MatchResultStatus.DRAW) {
            matchDetails.setResultStatus(matchResultStatus);
            matchDetails.setWinningTeamId(null);
            matchesRepository.saveAndFlush(matchDetails);
        } else {
            throw new IllegalArgumentException("Invalid result status " + resultStatus);
        }

        return ResponseEntity.ok().build();
    }

    @PutMapping("/matches/{id}/winning_team_id/{winningTeamId}")
    public ResponseEntity<Void> updateWinningTeam(@PathVariable Integer id, @PathVariable Integer winningTeamId) {
        Optional<MatchDetails> optionalMatchDetails = matchesRepository.findById(id);

        MatchDetails matchDetails = optionalMatchDetails.orElseThrow(() ->  new IllegalArgumentException("Invalid id " + id));

        if (!matchDetails.getTeam1Id().equals(winningTeamId) && !matchDetails.getTeam2Id().equals(winningTeamId)) {
            throw new IllegalArgumentException("Invalid winning team id " + winningTeamId);
        }

        matchDetails.setResultStatus(MatchResultStatus.RESULT);
        matchDetails.setWinningTeamId(winningTeamId);
        matchesRepository.saveAndFlush(matchDetails);

        return ResponseEntity.ok().build();
    }
}
