package com.ipl.matches.model;

public enum MatchResultStatus {
    RESULT,
    DRAW,
    ABANDONED;
}
