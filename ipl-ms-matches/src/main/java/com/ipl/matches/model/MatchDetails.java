package com.ipl.matches.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "match_details")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MatchDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "match_id")
    private Integer matchId;

    @Column(name = "team1_id")
    private Integer team1Id;

    @Column(name = "team2_id")
    private Integer team2Id;

    @Column(name = "winning_team_id")
    private Integer winningTeamId;

    @Column(name = "result_status")
    @Enumerated(EnumType.STRING)
    private MatchResultStatus resultStatus;

    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME, pattern = "yyyy-MM-ddTHH:mm:ss.sTZD")
    private Date startTime;

    @Column(name = "venue")
    private String venue;

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getTeam1Id() {
        return team1Id;
    }

    public void setTeam1Id(Integer team1Id) {
        this.team1Id = team1Id;
    }

    public Integer getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(Integer team2Id) {
        this.team2Id = team2Id;
    }

    public Integer getWinningTeamId() {
        return winningTeamId;
    }

    public void setWinningTeamId(Integer winningTeamId) {
        this.winningTeamId = winningTeamId;
    }

    public MatchResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(MatchResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    @Override
    public String toString() {
        return "MatchDetails{" +
                "matchId=" + matchId +
                ", team1Id=" + team1Id +
                ", team2Id=" + team2Id +
                ", winningTeamId=" + winningTeamId +
                ", resultStatus='" + resultStatus + '\'' +
                ", startTime=" + startTime +
                ", venue='" + venue + '\'' +
                '}';
    }
}
