package com.ipl.balances;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IplMsBalancesApplication {

	public static void main(String[] args) {
		SpringApplication.run(IplMsBalancesApplication.class, args);
	}

}
