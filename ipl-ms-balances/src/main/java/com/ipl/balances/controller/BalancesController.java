package com.ipl.balances.controller;

import com.ipl.balances.model.Balance;
import com.ipl.balances.model.BalanceRequest;
import com.ipl.balances.repository.BalancesRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class BalancesController {

    @Autowired
    private BalancesRepository balancesRepository;

    @GetMapping("/balance/{userId}")
    public ResponseEntity<Balance> getBalanceByUser(@PathVariable Integer userId) {
        Optional<Balance> optionalBalance = balancesRepository.findByUserId(userId);

        Balance balance = optionalBalance.orElseThrow(() ->  new IllegalArgumentException("Invalid user id " + userId));
        return ResponseEntity.ok(balance);
    }

    @PostMapping("/balance")
    public ResponseEntity<Balance> addBalanceForUser(@RequestBody BalanceRequest balanceRequest) {
        Balance balance = new Balance();
        BeanUtils.copyProperties(balanceRequest, balance);
        balancesRepository.save(balance);

        return ResponseEntity.ok().build();
    }
}
