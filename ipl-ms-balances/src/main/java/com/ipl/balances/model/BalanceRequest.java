package com.ipl.balances.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class BalanceRequest {

    @NotNull(message = "User id not supplied")
    private Integer userId;

    @NotNull(message = "Invested balance not supplied")
    private BigDecimal investedBalance;

    @NotNull(message = "Available balance not supplied")
    private BigDecimal availableBalance;
}
