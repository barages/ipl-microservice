package com.ipl.balances.repository;

import com.ipl.balances.model.Balance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BalancesRepository extends JpaRepository<Balance, Integer> {

    Optional<Balance> findByUserId(Integer userId);
}
