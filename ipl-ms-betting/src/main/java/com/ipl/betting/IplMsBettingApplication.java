package com.ipl.betting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IplMsBettingApplication {

	public static void main(String[] args) {
		SpringApplication.run(IplMsBettingApplication.class, args);
	}

}
